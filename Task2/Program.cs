﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Task2
{
    class Program
    {
        static SortedSet<string> termSet = new SortedSet<string>();
        static Dictionary<string, int> termVertex = new Dictionary<string, int>();
        static int[,] g;

        static void termTest(string term)
        {
            foreach (string t in termSet)
            {
                string first = "";
                string second = "";
                if (term.Contains("("))
                {
                    first = term.Substring(0, term.IndexOf('('));
                }
                else
                {
                    first = term;
                }
                if (t.Contains("("))
                {
                    second = t.Substring(0, t.IndexOf('('));
                }
                else
                {
                    second = t;
                }
                if (first == second && t != term)
                {
                    Console.WriteLine("Некорректный ввод");
                    Environment.Exit(2);
                }
            }
        }

        static void getTerms(string term)
        {
            termTest(term);
            termSet.Add(term);
            if (!(term.Contains("(") || term.Contains(")")))
            {
                return;
            }
            if (term.Contains("("))
            {
                if (term[term.Length - 1] != ')' || term[term.Length - 1] == '(')
                {
                    Console.WriteLine("Некорректный ввод");
                    Environment.Exit(2);
                }
                term = term.Substring(term.IndexOf('(') + 1, term.Length - term.IndexOf('(') - 2);
            }
            term += ",";
            int count = 0;
            int last_i = 0;
            for (int i = 0; i < term.Length; i++)
            {
                if (term[i] == '(') count++;
                if (term[i] == ')') count--;
                if (term[i] == ',' && count < 0)
                {
                    Console.WriteLine("Некорректный ввод");
                    Environment.Exit(2);
                }
                if (term[i] == ',' && count == 0)
                {
                    if (term.Substring(last_i, i - last_i) == "")
                    {
                        Console.WriteLine("Некорректный ввод");
                        Environment.Exit(2);
                    }
                    getTerms(term.Substring(last_i, i - last_i));
                    last_i = i + 1;

                }
            }
        }

        static void getEdges(string term)
        {
            string tmp = (string)term.Clone();
            if (!(term.Contains("(") || term.Contains(")")))
            {
                return;
            }
            if (term.Contains("("))
            {
                term = term.Substring(term.IndexOf('(') + 1, term.Length - term.IndexOf('(') - 2);
            }
            term += ",";
            int count = 0;
            int last_i = 0;
            int positionTerm = 1;
            for (int k = 0; k < term.Length; k++)
            {
                if (term[k] == '(') count++;
                if (term[k] == ')') count--;
                if (term[k] == ',' && count == 0)
                {
                    int i = termVertex[tmp];
                    int j = termVertex[term.Substring(last_i, k - last_i)];
                    g[i, j] = positionTerm++;
                    getEdges(term.Substring(last_i, k - last_i));
                    last_i = k + 1;

                }
            }
        }

        static void Main(string[] args)
        {
            string path = args.Length == 0 ? @"..\..\input.txt" : args[0];
            if (path == @"..\..\input.txt" && !File.Exists(path))
            {
                Console.WriteLine("Укажите файл");
                Environment.Exit(1);
            }

            if (!File.Exists(path))
            {
                Console.WriteLine("Такого файла не существует");
                Environment.Exit(1);
            }

            string[] lines = File.ReadAllLines(path);

            string firstTerm = System.Text.RegularExpressions.Regex.Replace(lines[0], @"\s+", "");

            int count = 0;
            for (int i = 0; i < firstTerm.Length; i++)
            {
                if (firstTerm[i] == '(') count++;
                if (firstTerm[i] == ')') count--;
            }

            if (count != 0)
            {
                Console.WriteLine("Некорректный ввод");
                Environment.Exit(2);
            }

            getTerms(firstTerm);

            string[] terms = termSet.ToArray();
            g = new int[terms.Length, terms.Length];

            for (int i = 0; i < terms.Length; i++)
            {
                termVertex.Add(terms[i], i);
            }

            getEdges(firstTerm);

            for (int i = 1; i < g.GetLength(0); i++)
            {
                for (int j = 0; j < i; j++)
                {
                    g[i, j] ^= g[j, i];
                    g[j, i] ^= g[i, j];
                    g[i, j] ^= g[j, i];
                }
            }

            for (int i = 0; i < terms.Length; i++)
            {
                string v = "";
                if (terms[i].Contains("("))
                {
                    v = terms[i].Substring(0, terms[i].IndexOf('('));
                }
                else
                {
                    v = terms[i];
                }
                Console.Write(v + " ");
            }
            Console.WriteLine();

            for (int i = 0; i < g.GetLength(0); i++)
            {
                for (int j = 0; j < g.GetLength(1); j++)
                {
                    Console.Write(g[i, j] + " ");
                }
                Console.WriteLine();
            }

        }
    }
}
