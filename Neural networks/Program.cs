﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Neural_networks
{
    class Program
    {
        static int[,] g;
        static int[] color;
        static string[] term;
        static string[] vertex; 

        static string getTerms(int v)
        {
            if (color[v] == 2)
            {
                return term[v];
            }

            if (color[v] == 1)  
            {
                Console.WriteLine("В графе присутствует цикл");
                Environment.Exit(2);
            }

            color[v] = 1;
            bool f = true;
            string[] positionTerm = new string[vertex.Length];
            for (int i = 0; i < g.GetLength(1); ++i)  
            {
                if (g[v, i] != 0)
                {
                    f = false;
                    if (term[i] == null)
                    {
                        positionTerm[g[v, i]] = getTerms(i);
                    }
                    else
                    {
                        positionTerm[g[v, i]] = term[i];
                    }
                }
            }

            color[v] = 2;

            if (f)
            {
                term[v] = vertex[v];
                return term[v];
            }

            string str = "";

            foreach (string s in positionTerm)
                if (s != null)
                    str += s + ",";

            str = vertex[v] + "(" + str.Substring(0, str.Length - 1) + ")";

            term[v] = str;

            return str;
        }

        static void Main(string[] args)
        {
            string path = args.Length == 0 ? @"..\..\input.txt" : args[0];
            if (path == @"..\..\input.txt" && !File.Exists(path))
            {
                Console.WriteLine("Укажите файл");
                Environment.Exit(1);
            }

            if (!File.Exists(path))
            {
                Console.WriteLine("Такого файла не существует");
                Environment.Exit(1);
            }

            string[] lines = System.IO.File.ReadAllLines(path);

            vertex = lines[0].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            g = new int[vertex.Length, vertex.Length];
            color = new int[vertex.Length];
            term = new string[vertex.Length];

            for (int i = 1; i < vertex.Length + 1; i++)
            {
                int[] arr = lines[i]
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(n => int.Parse(n))
                    .ToArray();
                for (int j = 0; j < arr.Length; j++)
                {
                    g[i - 1, j] = arr[j];
                }
            }

            for (int i = 1; i < g.GetLength(0); i++)
            {
                for (int j = 0; j < i; j++)
                {
                    g[i, j] ^= g[j, i];
                    g[j, i] ^= g[i, j];
                    g[i, j] ^= g[j, i];
                }
            }

            List<int> graphSource = new List<int>();
            for (int i = 0; i < g.GetLength(0); i++)
            {
                int s = 0;
                for (int j = 0; j < g.GetLength(1); j++)
                {
                    s += g[j, i];
                }
                if (s == 0)
                {
                    graphSource.Add(i);
                }
            }

            if (graphSource.Count == 0)
            {
                Console.WriteLine("В графе присутствует цикл");
                Environment.Exit(2);
            }

            foreach (int source in graphSource)
            {
                for (int i = 0; i < color.Length; i++)
                {
                    color[i] = 0;
                }

                getTerms(source);
            }

            for (int i = 0; i < term.Length; i++)
            {
                Console.WriteLine(vertex[i] + " : " + term[i]);
            }
        }
    }
}
